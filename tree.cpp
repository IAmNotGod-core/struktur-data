#include <iostream>

using namespace std;

struct Tree
{
    int data;

    Tree *left;
    Tree *right;
};

Tree *node, *root = NULL;

Tree *newNode(int idata)
{
    node = new Tree();
    node->data = idata;
    node->left = NULL;
    node->right = NULL;
    
    return node;
}

void preOrder(Tree *current)
{
    if (current != NULL)
    {
        cout << current->data << " ";
        preOrder(current->left);
        preOrder(current->right);
    }
    
}

void inOrder(Tree *current)
{
    if (current != NULL)
    {
        inOrder(current->left);
        cout << current->data << " ";
        inOrder(current->right);
    }
}

void postOrder(Tree *current)
{
    if (current != NULL)
    {
        postOrder(current->left);
        postOrder(current->right);
        cout << current->data << " ";
    }
}

void menu(int option)
{
    switch (option)
    {
    case 1:
        system("cls");
        cout << "Pre-Order: ";
        preOrder(root);
        cout << endl;
        break;
    
    case 2:
        system("cls");
        cout << "In-Order: ";
        inOrder(root);
        cout << endl;
        break;
    
    case 3:
        system("cls");
        cout << "Post-Order: ";
        preOrder(root);
        cout << endl;
        break;
        
    case 0:
        exit(0);
    
    default:
        cout << "Pilihan tidak tersedia!" << endl;
        break;
    }
}

int main()
{
    //Init Data
    root = newNode(21);
    root->left = newNode(13);
    root->right = newNode(36);
    
    root->left->left = newNode(8);
    root->left->right = newNode(15);

    root->left->left->left = newNode(6);
    root->left->left->right = newNode(11);

    root->left->right->right = newNode(18);
    
    root->right->left = newNode(26);
    root->right->right = newNode(17);
    
    root->right->right->right = newNode(4);
    
    int option;
    do
    {
        cout << "[1] Pre-Order" << endl;
        cout << "[2] In-Order" << endl;
        cout << "[3] Post-Order" << endl;
        cout << "[0] Exit" << endl;
        cout << "Masukkan pilihan : ";
        cin >> option;
        menu(option);
    } while (option != 0);
    
}

