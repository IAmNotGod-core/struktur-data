#include <iostream>

using namespace std;

#define MAX 8

typedef struct 
{
    int data[MAX];
    int head;
    int tail;
} Queue;

Queue antri;


void create();
void enqueue(int);
int dequeue();
void tampil();
void menu(int);

int main()
{
    int pilih;
    do
    {
        cout << "====================\n";
        cout << "[1] Create Queue\n";
        cout << "[2] Enqueue\n";
        cout << "[3] Dequeue\n";
        cout << "[4] Tampil Queue\n";
        cout << "====================\n";
        cin >> pilih;
        menu(pilih);
    } while (pilih != 0);
    return 0;
}

void create()
{
    antri.head = antri.tail = -1;
}

bool isEmpty()
{
    if (antri.tail == -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isFull()
{
    if (antri.tail == MAX-1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void enqueue(int data)
{
    if (isEmpty())
    {
        antri.head = antri.tail = 0;
        antri.data[antri.tail] = data;
    }
    else
    {
        antri.tail++;
        antri.data[antri.tail] = data;
    }
    cout << data << " Berhasil dimasukkan!\n";
}

int dequeue()
{
    int i;
    int e = antri.data[antri.head];
    for (i = antri.head; i <= antri.tail; i++)
    {
        antri.data[i] = antri.data[i+1];
    }
    antri.tail--;
    return e;
}

void tampil()
{
    if (isEmpty()==false)
    {
        for (int i = antri.head; i <= antri.tail; i++)
        {
            cout << "| " << antri.data[i] << " ";
        }
        cout << "|\n";
    }
    else
    {
        cout << "Data kosong!\n";
    }
}

void menu(int pil)
{
    switch (pil)
    {
    case 1:
        create();
        break;
    case 2:
        if (isFull())
        {
            cout << "Queue sudah penuh\n";
        }
        else
        {
            int data;
            cout << "Input value : ";
            cin >> data;
            enqueue(data);
        }
        break;
    case 3:
        dequeue();
        break;
    case 4:
        tampil();
        break;
    
    default:
        cout << "Invalid input!\n";
        break;
    }
}