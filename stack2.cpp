#include <iostream>
#include <string>

using namespace std;

#define MAX 24

void push(int);
char pop();
void print();
void resolve(string);
int top = -1;
char stk[MAX];

string msg = "HA***L*OAP***A*KAB*A***R";

int main()
{
    resolve(msg);
    print();
    return 0;
}

bool isFull()
{
    if (top == MAX-1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool isEmpty()
{
    if (top == -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void push(int i)
{
    if (isFull())
    {
        cout << "Stack Penuh";
    }
    else
    {
        top++;
        stk[top] = msg[i];
    }
}

char pop()
{
    char ch;
    if (isEmpty())
    {
        cout << "Stack Kosong";
    }
    else
    {
        ch = stk[top];
        stk[top] = '\0';
        top--;
        return (ch);
    }
    return 0;
}

void resolve(string msg)
{
    for (int i = 0; i < MAX; i++)
    {
        if(msg[i] >= 'A' && msg[i] <= 'Z')
        {
            push(i);
        }
    }
}

void print()
{
    for (int i = 0; i <= top; i++)
    {
        cout << stk[i] << " ";
    }
}