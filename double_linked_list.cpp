#include <iostream>

using namespace std;

struct TNode
{
    int data;

    TNode *prev;
    TNode *next;
};

TNode *head = NULL, *cur, *baru;

TNode *init1, *init2, *init3, *init4;

int isEmpty() {
  if (head == NULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

void insertDepan(int databaru) {
  baru = new TNode;
  baru->data = databaru;
  baru->next = NULL;
  baru->prev = NULL;
  if(isEmpty()==1)
  {
 	head=baru;
 	head->next = NULL;
 	head->prev = NULL;
  }
  else
  {
	baru->next = head;
	head->prev = baru;
	head = baru;
  }
  cout << "Data masuk" << endl;
}

void insertBelakang (int databaru) {
  baru = new TNode;
  baru->data = databaru;
  baru->next = NULL;
  baru->prev = NULL;
  if(isEmpty()==1){
    head=baru;
    head->next = NULL;
    head->prev = NULL;
  }
  else {
    cur=head;
    while(cur->next != NULL)
    {
      cur = cur->next;
    }
    cur->next = baru;
    baru->prev = cur;
  }
  cout << "Data masuk" << endl;
}

void deleteDepan()
{
  if (isEmpty())
  {
    cout << "Data kosong!\n";
  }
  else
  {
    TNode *hapus;
    hapus = head;
    head = hapus->next;
    head->prev = NULL;
    hapus->next = NULL;
    delete hapus;
    cout << "Data telah dihapus\n";
  }
}

void tampil()
{
  cur = head;
	if(isEmpty()==0)
    {
      while(cur != NULL)
      {
        cout << cur->data << " ";
        cur = cur->next;
      }
      cout << endl;
    }
    else
    {
        cout << "Data kosong!\n";
    }
}

void menu(int option)
{
  int input;
  switch (option)
  {
  case 1:
    system("cls");
    cout << "Tambah data di depan" << endl;
    cin >> input;
    insertDepan(input);
    break;
  
  case 2:
    system("cls");
    cout << "Tambah data di belakang" << endl;
    cin >> input;
    insertBelakang(input);
    break;

  case 3:
    system("cls");
    deleteDepan();
    break;

  case 4:
    system("cls");
    tampil();
    break;
  
  case 5:
    exit(0);

  default:
    cout << "Pilihan " << option << " tidak tersedia" << endl;
    break;
  }
}

int main()
{
  //Init dump data
  init1 = new TNode;
  init1->data = 2;
  init1->next = NULL;
  init1->prev = NULL;

  init2 = new TNode;
  init2->data = 7;
  init2->next = NULL;
  init2->prev = NULL;

  init3 = new TNode;
  init3->data = 10;
  init3->next = NULL;
  init3->prev = NULL;

  init4 = new TNode;
  init4->data = 4;
  init4->next = NULL;
  init4->prev = NULL;

  head = init1;
  init1->next = init2;
  init2->next = init3;
  init2->prev = init1;
  init3->next = init4;
  init3->prev = init2;
  init4->prev = init3;

  int option;
  do {
        cout << "1. Tambah Depan" <<endl;
        cout << "2. Tambah Belakang" <<endl;
        cout << "3. Hapus Depan" <<endl;
        cout << "4. Cetak List" <<endl;
        cout << "5. Keluar Program" <<endl;
        cout <<"Masukkan pilihan : ";
        cin >> option;
        menu(option);
    }
    while(option != 0);
    return 0;
}

